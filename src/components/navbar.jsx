import React from 'react';

const NavBar = props => {
    console.log('Navbar - Rendered');
    let url = "#";
    return (
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href={url}>
          Navbar{" "}
          <span className="badge badge-pill badge-secondary">
            {props.totalCounters}
          </span>
        </a>
      </nav>
    );
}
 
export default NavBar;

// import React, { Component } from "react";

// class NavBar extends Component {
//   render() {
    // {this.props.totalCounters}
//   }
// }
